# README projet RD 2021 - système de stockage objet

Ce dépôt Git sera mis à jour progressivement en tenant compte des différentes versions du système.

Sur la branche principale `clientserveur-def` se trouve l'implémentation quasi-finale du système de stockage objet en mode client/serveur.

Sur la branche secondaire et obsolète `old` se trouve les différents fichiers montrant les étapes de développement du système. Cette branche n'est plus maintenue et est obsolète.

## Mode d'emploi

L'architecture du modèle client/serveur s'articule autour d'un serveur qui expose les différentes entrées correspondant aux commandes possibles du système objet sous la forme d'une API REST.

Pour mettre en place cette version :
1. Exécuter le script `server.py` sur le serveur.

2. Les commandes possibles du client sont les suivantes :

```
python client.py -op put
python client.py -op get
python client.py -op delete
python client.py -op list_obj
python client.py -op list_version
python client.py -op set-limit
python client.py -op mget
python client.py -op list_key
python client.py -op list_exist_val
python client.py -op mput
python client.py -op mdel
python client.py -op list_part_key
python client.py -op list_part_val
python client.py -op list_key_regex
python client.py -op list_val_regex
```

3. L'argument `-u`, obligatoire, permet de préciser l'IP et le port d'écoute du serveur, sous la forme : http://ip:port ou http://nom-dns:port en cas de nom DNS.

Pour ces commandes, vous serez invités à entrer à la demande les différents paramètres.

Pour un transfert client vers le serveur facilité, on propose également une opération *batch* sous cette forme :
```
python client.py -op batch -b '{"op": "put", "name": "test11", "path": "/home/damien/newprojetrd-2021/README.md", "maxVersion": 3, "meta": {"val": "tes", "val2": "test2l"}}'
```

qui permet de transférer directement vers le système un objet nommé *test11*, situé dans le chemin absolu *path* du client, avec un nombre maximal de versions égal à 3 et les métadonnées associées.

> Attention ! L'argument de l'option -b doit obligatoirement être une string. D'autre part, le JSON fourni pour les métadonnées doit respecter les règles de syntaxe JSON.