import json
import requests
import argparse
import shutil
import os
import hashlib
import datetime

data = {'name': "obj1", 'maxVersion': 7, "meta": {'cle1': "test", 'cle2': 'test3'}}

filename = '../obj.txt'

# serveur distant
#url = "http://10.11.26.42:8080"

# Récupérer les arguments
parser = argparse.ArgumentParser()
parser.add_argument("-op","--operation", type=str, help="operation")
parser.add_argument("-b","--batch", type=str, help="batch", default="vide")
parser.add_argument("-u","--url", type=str, help="url", default="vide")

args = parser.parse_args()

# serveur distant
url = args.url

# Lire un fichier par chunk, avec une taille de 1 Mo par défaut
def read_in_chunks(file_object, chunk_size=1048576):
    while True:
        data = file_object.read(chunk_size)
        if not data:
            break
        yield data

# fonction d'envoi d'un chunk d'un fichier avec les métadonnées associées
def do_operation(dic_op):
    op = dic_op["op"]

    if op == "put":
        name = dic_op["name"]
        path = dic_op["path"]
        sha256 = dic_op["sha256"]
        version = dic_op["version"]
        maxVersion = dic_op["maxVersion"]
        metas = dic_op["meta"]

        data = {'name': name, 'maxVersion': maxVersion, 'version': version, 'sha256': sha256, "meta": metas}

        with open(path, 'rb') as f:
            for elt in read_in_chunks(f):
                files = [
                    ('document', (filename, elt, 'application/octet')),
                    ('data', ('data', json.dumps(data), 'application/json')),
                ]
                r = requests.put(url+"/put", files=files)
                print(r.content.decode('utf-8'))
        
        return "OK"


def ask_operation(operation):
    # PUT : Création d'une nouvelle version d'un objet
    if operation == "put":
        nameObject = input("Enter a name for your object : ")
        while True:
            pathObject = input("Enter the absolute path of your file containing the data : ")
            if os.access(pathObject, os.R_OK) and os.path.isfile(pathObject):
                break
            else:
                print("The path provided is not valid. Please retry.")
        
        while True:
            r = requests.get(url+"/maxVersion")
            maxVersion_sys = r.json()["maxVersion"]
            maxVersion_obj = input("Please set a maximum number of versions of the object ; press enter for the default number ("+maxVersion_sys+" versions) : ")
            if maxVersion_obj == "":
                maxVersion_obj = int(maxVersion_sys)
                break
            else:
                try:
                    maxVersion_obj = int(maxVersion_obj)
                except ValueError:
                    print("maxNumber is not a integer. Please retry.")
                    continue
                else:
                    break
        
        while True:
            metasObject = input("Please enter a JSON string containing the metadata for the object : ")
            try:
                metasObject = json.loads(metasObject)
            except:
                print("The string provided is not in JSON format. Please retry.")
                continue
            else:
                break
        
        with open(pathObject, 'rb') as f:
            sha256 = hashlib.sha256()
            while True:
                chunk = f.read(16 * 1024)
                if not chunk:
                    break
                sha256.update(chunk)
            sha256_obj = sha256.hexdigest()

        now = datetime.datetime.now()
        version = now.strftime("%d-%m-%Y,%H:%M:%S")

        data = {'op': "put", 'name': nameObject, 'path': pathObject, 'sha256': sha256_obj, 'maxVersion': maxVersion_obj, "version": version, "meta": metasObject}
        return do_operation(data)

    # GET : Récupération d'une version d'un objet (Dernière si version non précisée)
    elif operation == "get":
        nameObject = input("Enter a name for the object you want : ")
        r = requests.get(url+"/list?name="+nameObject)
        if r.status_code == 404:
            return "No object called "+nameObject+" in the system."
        else:
            print("List of all versions of the object "+nameObject)
            print(r.content.decode('utf-8'))
            while True:
                version = input("Select the desired version. Press Enter for the latest : ")
                if version == "":
                    break
                r = requests.get(url+"/search-full?name="+nameObject+"&version="+str(version))
                if r.status_code != 404:
                    break
                else:
                    print("Version "+version+" of the object "+nameObject+" not present in the system. Please retry.")
            
            if version == "":
                with requests.get(url+"/get?name="+nameObject, stream=True) as r:
                    with open(nameObject, 'wb') as f:
                        shutil.copyfileobj(r.raw, f)
                return "Latest version of the object "+nameObject+" successfully copied."
            else:
                with requests.get(url+"/get?name="+nameObject+"&version="+str(version), stream=True) as r:
                    with open(nameObject, 'wb') as f:
                        shutil.copyfileobj(r.raw, f)
                return "Version "+version+" of the object "+nameObject+" successfully copied."

    # SET-LIMIT : Définir le nombre de version max d'un objet (Tous les objets si aucune précision)
    elif operation == "set-limit":
        while True:
            objectName = input("Enter the object for which the maxVersion parameter must be set. For global maxVersion parameter, press Enter.")
            if objectName == "":
                break
            else:
                r = requests.get(url+"/search?name="+objectName)
                if r.status_code != 404:
                    break
                print("The object "+objectName+" is not present in the system.")
        while True:
            maxVersion_obj = input("Please set the maxVersion parameter : ")
            try:
                maxVersion_obj = int(maxVersion_obj)
            except ValueError:
                print("maxNumber is not a integer. Please retry.")
                continue
            else:
                break
        if objectName == "":
            r = requests.get(url+"/set-limit?limit="+str(maxVersion_obj))
            return "Setting global maxVersion at "+str(maxVersion_obj)
        else:
            r = requests.get(url+"/set-limit?name="+objectName+"&limit="+str(maxVersion_obj))
            return "Setting maxVersion at "+str(maxVersion_obj)+" for the object "+objectName

    # DELETE : Suppression d'une version d'un objet (toutes les versions si aucune précision)
    elif operation == "delete":
        while True:
            objectName = input("Enter the object to be deleted : ")
            r = requests.get(url+"/search?name="+objectName)
            if r.status_code != 404:
                break
            print("The object "+objectName+" is not present in the system.")
        while True:
            version = input("Select the desired version to be deleted. Press Enter to delete all versions of the object : ")
            if version == "":
                break
            r = requests.get(url+"/search-full?name="+objectName+"&version="+str(version))
            if r.status_code != 404:
                break
            else:
                print("Version "+version+" of the object "+objectName+" not present in the system. Please retry.")
        if version == "":
            r = requests.get(url+"/delete?name="+objectName)
            return "Deleting all versions of the object "+objectName+"..."
        else:
            r = requests.get(url+"/delete?name="+objectName+"&version="+str(version))
            return "Deleting version "+str(version)+" of the object "+objectName+"..."

    # LIST_OBJ : Lister les objets 
    elif operation == "list_obj":
        r = requests.get(url+"/list")
        return r.content.decode('utf-8')

    # LIST_VERSION : Lister les versions existantes d'un objet
    elif operation == "list_version":
        while True:
            objectName = input("Enter the object name : ")
            r = requests.get(url+"/search?name="+objectName)
            if r.status_code != 404:
                break
            print("The object "+objectName+" is not present in the system.")
        r = requests.get(url+"/list?name="+objectName)
        return r.content.decode('utf-8')

    # MGET : Afficher les clés et valeurs d'un objet
    elif operation == "mget":
        while True:
            objectName = input("Enter the object name : ")
            r = requests.get(url+"/search?name="+objectName)
            if r.status_code != 404:
                break
            print("The object "+objectName+" is not present in the system.")
        r = requests.get(url+"/mget?name="+objectName)
        return r.content.decode('utf-8')

    # BATCH : Effectuer l'opération décrite dans un format JSON
    elif operation == "batch":
        if args.batch == "vide":
            return "No JSON configuration provided."
        else:
            try:
                configObject = json.loads(args.batch)
            except:
                return "The configuration provided is not in JSON format."
            
            if not("op" in configObject) or not("name" in configObject) or not("path" in configObject):
                return "Configuration JSON is incomplete."
            
            op = configObject["op"]
            name = configObject["name"]
            path = configObject["path"]

            if "maxVersion" in configObject:
                maxVersion = configObject["maxVersion"]
            else:
                maxVersion = 5
            
            if "meta" in configObject:
                meta = configObject["meta"]
                print(type(meta))
                print(meta)
                try:
                    metas = json.dumps(meta)
                except:
                    return "The meta provided is not in JSON format. Please retry."
            else:
                meta = {}
        
            with open(path, 'rb') as f:
                sha256 = hashlib.sha256()
                while True:
                    chunk = f.read(16 * 1024)
                    if not chunk:
                        break
                    sha256.update(chunk)
                sha256_obj = sha256.hexdigest()
                
            now = datetime.datetime.now()
            version = now.strftime("%d-%m-%Y,%H:%M:%S")

            data = {'op': "put", 'name': name, 'path': path, 'sha256': sha256_obj, 'maxVersion': maxVersion,"version": version,  "meta": meta}
            return do_operation(data)
    
    # LIST-KEY : Lister des objets en fonction de l'existence d'une clé
    elif operation == "list_key":
        key = input("Please enter the key : ")
        r = requests.get(url+"/list-key?key="+key)
        if r.status_code == 404:
            return "No object present with key "+key
        else:
            return r.content.decode('utf-8')
    
    # LIST-EXIST-VAL-KEY : Lister des objets en fonction de l'existence d'une valeur pour une clé
    elif operation == "list_exist_val":
        key = input("Please enter the key : ")
        value = input("Please enter the value associated with key "+key+" : ")
        r = requests.get(url+"/list-exist-val-key?key="+key+"&val="+value)
        if r.status_code == 404:
            return "No object present with value = "+value+" linked to key "+key
        else:
            return r.content.decode('utf-8')

    # MPUT : Ajouter une clé/valeur sur un objet existant
    elif operation == "mput":
        while True:
            objectName = input("Enter the object name : ")
            r = requests.get(url+"/search?name="+objectName)
            if r.status_code != 404:
                break
            print("The object "+objectName+" is not present in the system.")
        key = input("Please enter the key to add : ")
        value = input("Please enter the value associated with "+key+" ")
        r = requests.get(url+"/mput?name="+objectName+"&key="+key+"&value="+value)
        if r.status_code == 404:
            return "Error in adding key/value ("+key+","+value+")"
        else:
            return r.content.decode('utf-8')

    # MDEL : Supprimer une clé sur un objet existant
    elif operation == "mdel":
        while True:
            objectName = input("Enter the object name : ")
            r = requests.get(url+"/search?name="+objectName)
            if r.status_code != 404:
                break
            print("The object "+objectName+" is not present in the system.")
        key = input("Please enter the key to delete : ")
        r = requests.get(url+"/mdel?name="+objectName+"&key="+key)
        if r.status_code == 404:
            return "Error in deleting key "+key
        else:
            return r.content.decode('utf-8')
    
    # LIST-PART-KEY : Lister des objets en fonction d'une partie de clé
    elif operation == "list_part_key":
        partkey = input("Please enter a string that must be contained in the metadata key of desired objects : ")
        r = requests.get(url+"/list-part-key?partkey="+partkey)
        if r.status_code == 404:
            return "The substring "+partkey+" is not present in any key of the objects."
        else:
            return r.content.decode('utf-8')
    
    # LIST-PART-VAL : Lister des objets en fonction d'une partie de valeur pour toute ou une partie de clé
    elif operation == "list_part_val":
        partkey = input("Please enter a string that must be contained in the metadata key of desired objects. To search with all objects, press Enter : ")
        if partkey == "":
            partkey = "vide"
        partval = input("Please enter a string that must be contained in the metadata value of desired objects : ")
        
        r = requests.get(url+"/list-part-val?partkey="+partkey+"&partval="+partval)
        if r.status_code == 400:
            return "The substring "+partval+" is not present in any key of the objects."
        elif r.status_code == 404:
            return "No object with key containing "+partkey+" and value containing "+partval+"."
        else:
            return r.content.decode('utf-8')
    
    # LIST-KEY-REGEX : Lister des objets en fonction d'une regex sur une clé
    elif operation == "list_key_regex":
        partkey = input("Please enter a regex for the key : ")
        r = requests.get(url+"/list-key-regex?partkey="+partkey)
        if r.status_code == 404:
            return "No object with matching regex "+partkey+" on keys."
        else:
            return r.content.decode('utf-8')

    # LIST-VAL-REGEX : Lister des objets en fonction d'une regex sur une valeur et d'une regex sur une clé
    elif operation == "list_val_regex":
        partkey = input("Please enter a regex for the key : ")
        partval = input("Please enter a regex for the value : ")
        
        r = requests.get(url+"/list-val-regex?partkey="+partkey+"&partval="+partval)
        if r.status_code == 404:
            return "No object with matching regex "+partkey+" on key and "+partval+" on value."
        else:
            return r.content.decode('utf-8')
    
res = ask_operation(args.operation)
print(res)