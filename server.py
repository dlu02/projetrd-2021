import os
import glob
import json
from flask import Flask, request, send_file, abort, jsonify, after_this_request
from werkzeug.utils import secure_filename
import datetime
import hashlib
from itertools import chain
import re

# ajout du listing des objets existants et des versions existantes des objets

# Répertoire où sont stockés les objets
#objectsPath = "/home/damien/obj"
# Modification du chemin des objets pour qu'il soit plus générique à partir de l'emplacement actuel du script python
objPath = os.getcwd() + "/obj"
objPath = objPath.replace("\\","/")
# On crée le dossier s'il n'existe pas
if not os.path.exists(objPath):
    os.makedirs(objPath)

# Répertoire où sont stockés les informations sur les objets
objMetadataPath = os.getcwd() + "/objmeta"
objMetadataPath = objMetadataPath.replace("\\","/")
if not os.path.exists(objMetadataPath):
    os.makedirs(objMetadataPath)

# Répertoire temporaire des fichiers
objTempPath = os.getcwd() + "/temp-file"
objTempPath = objMetadataPath.replace("\\","/")
if not os.path.exists(objTempPath):
    os.makedirs(objTempPath)


# nombre maximum de versions
maxVersion = 5

# def list_obj_vers(objectName):
#     # Le replace ("\\","/") permet d'éviter certains problèmes sur windows qui utiliserait "\\" à la place de "/"
#     matching_files = [ elt.replace("\\","/") for elt in glob.glob(objectsPath+"/"+objectName+"-v*")]
#     list_versions = [elt.replace(objectsPath+"/"+objectName+"-","") for elt in matching_files]
#     return ' -> '.join(list_versions)

app = Flask(__name__)

# retourne True si le fichier de données avec un hash de sha256 est présent dans au moins une version d'un objet du système et False sinon
def checkIfSha256IsUsed(sha256,nameObj):
    # lister tous les objets présents
    files = [os.path.splitext(filename)[0] for filename in os.listdir(objMetadataPath)]
    files.remove(nameObj)
    for elt2 in files:
        with open(objMetadataPath+"/"+elt2,'r') as f:
            metadata_obj_temp = json.loads(f.readline())
            dic_versions_temp = metadata_obj_temp["versions"]
            list_files_temp = list(dic_versions_temp.values())
            if sha256 in list_files_temp:
                return True
    return False

# retourne True si le fichier de données avec un hash de sha256 est présent dans au moins une version d'un objet du système excepté la version version de l'objet nameObj et False sinon
def checkIfChunkIsUsed(sha256,nameObj,version):
    # lister tous les objets présents
    files = [os.path.splitext(filename)[0] for filename in os.listdir(objMetadataPath)]
    for elt2 in files:
        if elt2 == nameObj:
            with open(objMetadataPath+"/"+elt2,'r') as f:
                metadata_obj_temp = json.loads(f.readline())
                dic_versions_temp = metadata_obj_temp["versions"]
                dic_versions_temp.pop(version)
                list_files_temp = list(dic_versions_temp.values())
                list_chunks = list(chain.from_iterable([elt["list"] for elt in list_files_temp]))
                if sha256 in list_chunks:
                    return True
        else:
            with open(objMetadataPath+"/"+elt2,'r') as f:
                metadata_obj_temp = json.loads(f.readline())
                dic_versions_temp = metadata_obj_temp["versions"]
                list_files_temp = list(dic_versions_temp.values())
                list_chunks = list(chain.from_iterable([elt["list"] for elt in list_files_temp]))
                if sha256 in list_chunks:
                    return True
    return False

# PUT : Création d'une nouvelle version d'un objet
@app.route('/put',methods=['PUT'])            
def put_file():   
    
    data = json.load(request.files['data'])
    objName = data["name"]
    metas = data["meta"]
    sha256_client = data["sha256"]
    version = data["version"]

    # recherche des objets avec le même nom déjà présents dans le système
    matching_files = [os.path.splitext(filename)[0] for filename in os.listdir(objMetadataPath+"/")]

    # cas 1 : l'objet n'est pas présent dans le système
    if not (objName in matching_files):

        # vérifier si un chunk identique est déjà présent
        files = [os.path.splitext(filename)[0] for filename in os.listdir(objPath+"/")]

        # si le chunk n'est pas présent dans le système, on transfère le chunk du client vers le serveur, sinon on ne transfère pas et on ne copie pas le chunk du client
        
        uploaded_file = request.files['document'].read()
        # calcul du sha256 du chunk
        sha256 = hashlib.sha256()
        sha256_chunk = sha256.update(uploaded_file)
        sha256_chunk = sha256.hexdigest()

        if not (sha256_client in files):
            # écriture du chunk
            with open(objPath+"/"+sha256_chunk, 'ab+') as f:
                f.write(uploaded_file)
        
        # écrire la limite de versions selon qu'elle a été fournie par l'utilisateur ou non
        if "maxVersion" in data:
            dict = {"name": objName, "maxVersion": str(data["maxVersion"]), "countVer": "1", "versions": {version: {"full": sha256_client, "list": [sha256_chunk]}}, "metas": metas}
        else:
            dict = {"name": objName, "countVer": "1", "versions": {version: {"full": sha256_client, "list": [sha256_chunk]}}, "metas": metas}
        
        # écrire le fichier des métadonnées de l'objet
        with open(objMetadataPath+"/"+objName, 'a+') as f:
            f.write(json.dumps(dict))
        return "Object called "+objName+" successfully copied in the object file system. Version "+version

    # cas 2 : l'objet est présent dans le système (ie. il existe un fichier de métadonnées nommé objName)
    else:
        # récupérer le maxVersion éventuellement fixé par l'utilisateur à partir des métadonnées
        f = open(objMetadataPath+"/"+objName, 'r')
        metadata_obj = json.loads(f.readline())
        countVer = int(metadata_obj["countVer"])

        # si on a déjà put au moins un chunk, on continue à put les chunks suivants
        if (version in metadata_obj["versions"]):
            uploaded_file = request.files['document'].read()
            # calcul du sha256 du chunk reçu
            sha256 = hashlib.sha256()
            sha256_chunk = sha256.update(uploaded_file)
            sha256_chunk = sha256.hexdigest()

            # vérifier si un chunk identique est déjà présent
            files = [os.path.splitext(filename)[0] for filename in os.listdir(objPath+"/")]

            # si un chunk identique est déjà présent dans le système, pas de réception du fichier du client, écriture uniquement de la version uploadée, sinon réception du fichier du client en plus
            if not(sha256_chunk in files):
                # écriture du chunk
                with open(objPath+"/"+sha256_chunk, 'ab+') as f:
                    f.write(uploaded_file)

            metadata_obj["versions"][version]["list"].append(sha256_chunk)

            with open(objMetadataPath+"/"+objName, 'w') as f:
                f.write(json.dumps(metadata_obj))
            
            return "Chunk with SHA256 "+sha256_chunk+" successfully uploaded."

        # ce cas se produit si on put le 1er chunk de la version de l'objet obj
        else:
            try:
                maxVersion_f = metadata_obj["maxVersion"]
            except:
                maxVersion_f = str(maxVersion)
            print(maxVersion_f)
            f.close()
            
            # si le nombre de versions est égal à maxVersion_f, on supprime la plus vieille version de l'objet (a) et on upload le premier chunk de l'objet qui sera en dernière version (b)
            if countVer==int(maxVersion_f):
                print("Warning ! Creating "+str(countVer+1)+"th version of the object "+objName+". Deleting the oldest version...")

                # (a) supprimer la plus vieille version de l'objet

                with open(objMetadataPath+"/"+objName, 'r') as f:
                    metadata_obj = json.loads(f.readline())
                    dic_versions = metadata_obj["versions"]

                # récupérer la liste des versions de l'objet
                list_versions = dic_versions.keys()
                list_versions = sorted(list_versions, key=lambda x: datetime.datetime.strptime(x, '%d-%m-%Y,%H:%M:%S'))

                # liste des chunks correspondant à la version de l'objet la plus vieille
                list_chunk_v_to_delete = dic_versions[list_versions[0]]["list"]

                # suppression du chunk s'il n'est pas utilisé dans aucune autre version de l'objet ou dans aucun autre objet
                for elt in list_chunk_v_to_delete:
                    if not checkIfChunkIsUsed(elt,objName,list_versions[0]):
                        os.remove(objPath+"/"+elt)
                
                # retrait de la version dans les métadonnées
                dic_versions.pop(list_versions[0])


                # (b) upload du premier chunk de la dernière version de l'objet

                uploaded_file = request.files['document'].read()
                # calcul du sha256 du chunk reçu
                sha256 = hashlib.sha256()
                sha256_chunk = sha256.update(uploaded_file)
                sha256_chunk = sha256.hexdigest()

                # vérifier si un chunk identique est déjà présent
                files = [os.path.splitext(filename)[0] for filename in os.listdir(objPath+"/")]

                # si un chunk identique est déjà présent dans le système, pas de réception du fichier du client, écriture uniquement de la version uploadée, sinon réception du fichier du client en plus
                if not(sha256_chunk in files):
                    # écriture du chunk
                    with open(objPath+"/"+sha256_chunk, 'ab+') as f:
                        f.write(uploaded_file)
                
                # ajout du dictionnaire correspondant à la dernière version
                try:
                    dic_versions[version]["list"]
                except:
                    dic_versions[version] = {"full": sha256_client, "list": [sha256_chunk]}
                else:
                    dic_versions[version]["list"].append(sha256_chunk)
                
                metadata_obj["versions"] = dic_versions
                
                with open(objMetadataPath+"/"+objName, 'w') as f:
                    f.write(json.dumps(metadata_obj))
                return "Object called "+objName+" successfully copied in the object file system. Version "+version

            
            # si le nombre de versions n'est pas égal à maxVersion_f, on upload si besoin le premier chunk correspondant à la version uploadée et on met à jour le fichier des métadonnées avec ladite version
            else:
                uploaded_file = request.files['document'].read()
                # calcul du sha256 du chunk
                sha256 = hashlib.sha256()
                sha256_chunk = sha256.update(uploaded_file)
                sha256_chunk = sha256.hexdigest()

                # vérifier si un chunk identique est déjà présent
                files = [os.path.splitext(filename)[0] for filename in os.listdir(objPath+"/")]

                # si un chunk identique est déjà présent dans le système, pas de réception du fichier du client, écriture uniquement de la version uploadée, sinon réception du fichier du client en plus
                if not(sha256_chunk in files):
                    # écriture du chunk
                    with open(objPath+"/"+sha256_chunk, 'ab+') as f:
                        f.write(uploaded_file)

                metadata_obj["versions"][version] = {"full": sha256_client, "list": [sha256_chunk]}
                metadata_obj["countVer"] = int(metadata_obj["countVer"]) + 1

                with open(objMetadataPath+"/"+objName, 'w') as f:
                    f.write(json.dumps(metadata_obj))
                
                return "Chunk with SHA256 "+sha256_chunk+" successfully uploaded."


# GET : Récupération d'une version d'un objet (Dernière si version non précisée)
@app.route('/get',methods=['GET'])            
def get_file():
    objName = request.args.get('name')

    # recherche des objets contenant le nom objectName
    matching_files = [ e.replace("\\","/") for e in glob.glob(objMetadataPath+"/"+objName)]

    # cas où l'objet n'est pas présent dans le système
    if len(matching_files) == 0:
        abort(404)

    # si la version à obtenir n'est pas précisée
    if not ("version" in request.args):
        # lister toutes les versions de l'objet objectName présentes dans le système
        with open(objMetadataPath+"/"+objName,'r') as f:
            metadata_obj = json.loads(f.readline())
            dic_versions = metadata_obj["versions"]

        list_versions = dic_versions.keys()
        list_versions = sorted(list_versions, key=lambda x: datetime.datetime.strptime(x, '%d-%m-%Y,%H:%M:%S'))

        # récupérer la version la plus récente
        last_version = list_versions[-1]

        print("Object called "+objName+" successfully found in version "+last_version)

        list_chunks = dic_versions[last_version]["list"]
        sha256_full = dic_versions[last_version]["full"]

        with open(sha256_full,"ab") as tmp:
            for elt in list_chunks:
                with open(objPath+"/"+elt,"rb") as chk:
                    tmp.write(chk.read())

        @after_this_request
        def delete_temp(response):
            os.remove(sha256_full)
            return response
        
        return send_file(sha256_full, download_name=objName, as_attachment=True)
        
    else:
        reqVersion = request.args.get('version')

        with open(objMetadataPath+"/"+objName,'r') as f:
            metadata_obj = json.loads(f.readline())
            dic_versions = metadata_obj["versions"]

        # si la version demandée n'est pas présente dans le système
        try:
            objCopy = dic_versions[reqVersion]
        except:
            return "No object called "+objName+" in the version "+reqVersion+"."
        else:
            # copier la version demandée de l'objet
            print("Object called "+objName+" successfully found in version "+reqVersion)

            # lister les chunks composant la version demandée de l'objet
            list_chunks = objCopy["list"]
            sha256_full = objCopy["full"]

            # concaténer les chunks entre eux dans un fichier temporaire
            with open(sha256_full,"ab") as tmp:
                for elt in list_chunks:
                    with open(objPath+"/"+elt,"rb") as chk:
                        tmp.write(chk.read())
            
            # suppression du fichier temporaire côté serveur après la fin de la requête
            @after_this_request
            def delete_temp(response):
                os.remove(sha256_full)
                return response

            return send_file(sha256_full, as_attachment=True)

# DELETE : Suppression d'une version d'un objet (toutes les versions si aucune précision)
@app.route('/delete',methods=['GET'])            
def delete_file():
    objName = request.args.get('name')

    # recherche des objets contenant le nom objectName
    matching_files = [ e.replace("\\","/") for e in glob.glob(objMetadataPath+"/"+objName)]

    # cas où l'objet n'est pas présent dans le système
    if len(matching_files) == 0:
        return "No object called "+objName+" in the object file system."
        
    # si la version à supprimer n'est pas précisée
    if not ("version" in request.args):
        with open(objMetadataPath+"/"+objName,'r') as f:
            metadata_obj = json.loads(f.readline())
            dic_versions = metadata_obj["versions"]
        
        for version in dic_versions:
            list_chunks = dic_versions[version]["list"]
            for elt in list_chunks:
                if not checkIfChunkIsUsed(elt,objName,version):
                    os.remove(objPath+"/"+elt)
        
        os.remove(objMetadataPath+"/"+objName)
        return "Objects called "+objName+" deleted in all versions."

    # si la version à supprimer est précisée  
    else:
        version = request.args.get('version')
        with open(objMetadataPath+"/"+objName,'r') as f:
            metadata_obj = json.loads(f.readline())
            dic_versions = metadata_obj["versions"]
        
        try:
            versionToDelete = dic_versions[version]
        except:
            return "Version "+version+" of object "+objName+" not found."
        else:
            list_chunk_v_to_delete = versionToDelete["list"]

            for elt in list_chunk_v_to_delete:
                print(checkIfChunkIsUsed(elt,objName,version))
                if not checkIfChunkIsUsed(elt,objName,version):
                    os.remove(objPath+"/"+elt)
            
            if int(metadata_obj["countVer"])==1:
                os.remove(objMetadataPath+"/"+objName)
            else:
                dic_versions.pop(version)
                metadata_obj["versions"] = dic_versions
                metadata_obj["countVer"] = str(int(metadata_obj["countVer"]) - 1)

                with open(objMetadataPath+"/"+objName,'w') as f:
                    f.write(json.dumps(metadata_obj))

            return "Version "+version+" of object "+objName+" succesfully deleted."


# LIST : Lister les versions existantes d'un objet (Tous les objets si aucune précision)
@app.route('/list',methods=['GET'])            
def list_versions():
    if not ("name" in request.args):
        res = {}
        # Lister tous les objets présents
        files = [os.path.splitext(filename)[0] for filename in os.listdir(objMetadataPath)]
        for elt in files:
            with open(objMetadataPath+"/"+elt,"r") as f:
                metadata_obj = json.loads(f.readline())
                dic_versions = metadata_obj["versions"]
            res[elt] = dic_versions
        return json.dumps(res)
    else: 
        objName = request.args.get("name")
        # Recherche des objets contenant le nom objectName
        matching_files = [ e.replace("\\","/") for e in glob.glob(objMetadataPath+"/"+objName)]
        if matching_files==[]:
            abort(404)
        else:
            with open(matching_files[0], 'r') as f:
                metadata_obj = json.loads(f.readline())
                dic_versions = metadata_obj["versions"].keys()
            return jsonify(list(dic_versions))

# SET-LIMIT : Définir le nombre de version max d'un objet (Tous les objets si aucune précision)
@app.route('/set-limit',methods=['GET'])
def set_limit():
    if not ("name" in request.args) and not ("limit" in request.args):
        return "No object name and no maxNumber limit provided for set-limit operation."
    
    if not ("limit" in request.args) and "name" in request.args:
        return "No limit provided for set-limit operation."
    
    if not ("name" in request.args) and "limit" in request.args:
        global maxVersion
        maxVersion = request.args.get("limit")
        return "Setting global maxVersion limit at "+maxVersion
    else:
        objName = request.args.get("name")
        limit = request.args.get("limit")

        matching_files = [ e.replace("\\","/") for e in glob.glob(objMetadataPath+"/"+objName)]
        if len(matching_files) == 0:
            return "No object found with name "+objName
        else:
            with open(objMetadataPath+"/"+objName, 'r+') as f:
                meta = json.loads(f.readline())
                meta["maxVersion"] = int(limit)

            with open(objMetadataPath+"/"+objName, 'w') as f:
                f.write(json.dumps(meta))
                
            return "Setting maxVersion limit at "+limit+" for object "+objName+". Success."

# SEARCH-FULL : Vérifier si un objet ayant le nom et la version donnés existe 
@app.route('/search-full',methods=['GET'])            
def search_full():
    objName = request.args.get('name')

    # recherche des objets contenant le nom objectName
    matching_files = [ e.replace("\\","/") for e in glob.glob(objMetadataPath+"/"+objName)]

    # cas où l'objet n'est pas présent dans le système
    if len(matching_files) == 0:
        abort(404)

    reqVersion = request.args.get('version')

    with open(objMetadataPath+"/"+objName, 'r') as f:
        metadata_obj = json.loads(f.readline())
        dic_versions = metadata_obj["versions"]
    
    try:
        objReqVersion = dic_versions[reqVersion]
    except:
        abort(404)
    else:
        return "OK"

# SEARCH : Vérifier si un objet ayant le nom donné existe 
@app.route('/search',methods=['GET'])            
def search_obj():
    objName = request.args.get('name')

    # recherche des objets contenant le nom objectName
    matching_files = [ e.replace("\\","/") for e in glob.glob(objMetadataPath+"/"+objName)]

    # cas où l'objet n'est pas présent dans le système
    if len(matching_files) == 0:
        abort(404)
    return "OK"

# MGET : Affichage des clés et valeurs d'un objet
@app.route('/mget',methods=['GET'])            
def mget():
    objName = request.args.get('name')

    # recherche des objets contenant le nom objectName
    matching_files = [ e.replace("\\","/") for e in glob.glob(objMetadataPath+"/"+objName)]

    # cas où l'objet n'est pas présent dans le système
    if len(matching_files) == 0:
        abort(404)
    else:
        with open(objMetadataPath+"/"+objName, 'r') as f:
            dict = f.read().rstrip()
        dict = json.loads(dict)
        return json.dumps(dict["metas"], indent=1)

# LIST-KEY : Lister des objets en fonction de l'existence d'une clé
@app.route('/list-key',methods=['GET'])
def list_key():
    key = request.args.get('key')
    l = [ e.replace("\\","/") for e in glob.glob(objMetadataPath+"/*")]
    meta_list = []
    for elt in l:
        with open(elt, 'r+') as f:
            jso = json.loads(f.readline())
            meta = jso["metas"]
            name = jso["name"]
            if key in meta:
                meta_list.append(name)
    if meta_list == []:
        abort(404)
    else:
        return jsonify(meta_list)

# LIST-EXIST-VAL-KEY : Lister des objets en fonction de l'existence d'une valeur pour une clé
@app.route('/list-exist-val-key',methods=['GET'])
def list_exist_val():
    key = request.args.get('key')
    val = request.args.get('val')
    l = [ e.replace("\\","/") for e in glob.glob(objMetadataPath+"/*")]
    meta_list = []
    for elt in l:
        with open(elt, 'r+') as f:
            jso = json.loads(f.readline())
            meta = jso["metas"]
            name = jso["name"]
            try:
                test = meta[key]
            except:
                continue
            else:
                if test == val:
                    meta_list.append(name)
    if meta_list == []:
        abort(404)
    else:
        return jsonify(meta_list)

# MPUT : Ajouter une clé/valeur sur un objet existant
@app.route('/mput',methods=['GET'])
def mput():
    key = request.args.get('key')
    value = request.args.get('value')
    name = request.args.get('name')

    try:
        with open(objMetadataPath+"/"+name,'r+') as f:
            jso = json.loads(f.readline())
            meta = jso["metas"]
            meta[key] = value
            jso["metas"] = meta
        with open(objMetadataPath+"/"+name,'w') as f:
            f.write(json.dumps(jso))
        return "Successfully added key/value."
    except:
        abort(404)

# MDEL : Supprimer une clé sur un objet existant
@app.route('/mdel',methods=['GET'])
def mdel():
    key = request.args.get('key')
    name = request.args.get('name')

    try:
        with open(objMetadataPath+"/"+name,'r+') as f:
            jso = json.loads(f.readline())
            meta = jso["metas"]
            try:
                meta.pop(key)
            except:
                return "Key is not present in the metadata."
            else:
                jso["metas"] = meta
        with open(objMetadataPath+"/"+name,'w') as f:
            f.write(json.dumps(jso))
        return "Successfully deleted key "+key+" from object "+name
    except:
        return "Object is not present."

# LIST-PART-KEY : Lister des objets en fonction d'une partie de clé
@app.route('/list-part-key',methods=['GET'])
def list_part_key():
    partkey = request.args.get('partkey')
    l = [ e.replace("\\","/") for e in glob.glob(objMetadataPath+"/*")]
    meta_list = []
    for elt in l:
        with open(elt, 'r+') as f:
            jso = json.loads(f.readline())
            meta = jso["metas"]
            name = jso["name"]
            key_list = meta.keys()
            for elt in key_list:
                if partkey in elt:
                    meta_list.append(name)
                    break
    if meta_list == []:
        abort(404)
    else:
        return jsonify(meta_list)

# LIST-PART-VAL : Lister des objets en fonction d'une partie de valeur pour toute ou une partie de clé
@app.route('/list-part-val',methods=['GET'])
def list_part_val():
    partval = request.args.get('partval')
    partkey = request.args.get('partkey')
    if partkey == "vide":
        l = [ e.replace("\\","/") for e in glob.glob(objMetadataPath+"/*")]
        meta_list = []
        for elt in l:
            with open(elt, 'r+') as f:
                jso = json.loads(f.readline())
                meta = jso["metas"]
                name = jso["name"]
                print(meta)
                for elt in meta:
                    if partval in meta[elt]:
                        meta_list.append(name)
                        break
        if meta_list == []:
            abort(400)
        else:
            return jsonify(meta_list)
    else:
        l = [ e.replace("\\","/") for e in glob.glob(objMetadataPath+"/*")]
        meta_list = []
        for elt in l:
            with open(elt, 'r+') as f:
                jso = json.loads(f.readline())
                meta = jso["metas"]
                name = jso["name"]
                for elt in meta:
                    if partkey in elt:
                        if partval in meta[elt]:
                            meta_list.append(name)
                            break
        if meta_list == []:
            abort(404)
        else:
            return jsonify(meta_list)

# LIST-KEY-REGEX : Lister des objets en fonction d'une regex sur une clé
@app.route('/list-key-regex',methods=['GET'])
def list_key_regex():
    partkey = request.args.get('partkey')
    l = [ e.replace("\\","/") for e in glob.glob(objMetadataPath+"/*")]
    meta_list = []
    for elt in l:
        with open(elt, 'r+') as f:
            jso = json.loads(f.readline())
            meta = jso["metas"]
            name = jso["name"]
            for elt in meta:
                if re.search(partkey,elt):
                    meta_list.append(name)
                    break
    if meta_list == []:
        abort(404)
    else:
        return jsonify(meta_list)

# LIST-VAL-REGEX : Lister des objets en fonction d'une regex sur une valeur et d'une regex sur une clé
@app.route('/list-val-regex',methods=['GET'])
def list_val_regex():
    partval = request.args.get('partval')
    partkey = request.args.get('partkey')
    print(partval)
    l = [ e.replace("\\","/") for e in glob.glob(objMetadataPath+"/*")]
    meta_list = []
    for elt in l:
        with open(elt, 'r+') as f:
            jso = json.loads(f.readline())
            meta = jso["metas"]
            name = jso["name"]
            for elt in meta:
                if re.search(partkey,elt):
                    if re.search(partval,meta[elt]):
                        meta_list.append(name)
                        break
    if meta_list == []:
        abort(404)
    else:
        return jsonify(meta_list)


# auxiliaire : retourne le paramètre maxVersion global par défaut
@app.route('/maxVersion',methods=['GET'])
def maxVersionReq():
    return jsonify({"maxVersion": str(maxVersion)})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
