#!/bin/bash

SERVER_URL='http://172.17.0.50:8080/'
SERVER_URL2='http://projetrd.dlu02.ovh:8080/'
time=2
time2=0
put="python client.py -op put -u ${SERVER_URL}" 
get="python client.py -op get -u ${SERVER_URL}"
delete="python client.py -op delete -u ${SERVER_URL}"
list_obj="python client.py -op list_obj -u ${SERVER_URL}"
list_version="python client.py -op list_version -u ${SERVER_URL}"
set_limit="python client.py -op set-limit -u ${SERVER_URL}"
mget="python client.py -op mget -u ${SERVER_URL}"
list_key="python client.py -op list_key -u ${SERVER_URL}"
list_exist_val="python client.py -op list_exist_val -u ${SERVER_URL}"
mput="python client.py -op mput -u ${SERVER_URL}"
mdel="python client.py -op mdel -u ${SERVER_URL}"
list_part_key="python client.py -op list_part_key -u ${SERVER_URL}"
list_part_val="python client.py -op list_part_val -u ${SERVER_URL}"
list_key_regex="python client.py -op list_key_regex -u ${SERVER_URL}"
list_val_regex="python client.py -op list_val_regex -u ${SERVER_URL}"


echo "Tests des fonctionnalités du serveur en faisant appel à client.py "
echo "Les fichiers de test sont contenus dans le dossier Tests "
echo "> Les différents paramètres/tests possibles sont : put, metadata, maxVersion, delete, get, deduplication, bash"
echo ">> Pour exécuter tous les tests utilisez le paramètre 'all'"
echo ">> Utilisez le paramètre clean, pour supprimer tous les fichiers du serveur"
echo "============================================"

put_test () {
    echo "PUT :"
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "Création des objets de noms 'TestPut1', 'TestPut2' et 'TestPut3' dont les données sont contenues dans les fichiers testPut1.txt, testPut2.txt et testPut3.txt  "
    echo "Les objets ont pour métadonnées : {'BashTest':'Put', 'Type': 'Put1'} {'BashTest':'Put','TypeTest': 'Put2'} et {'BashTest':'Put','TypeTest': 'Put3'}"
    echo "TestPut1 a comme nombre de versions max le nombre de base : 5"
    echo "TestPut2 a comme nombre de versions max : 2"
    echo "TestPut3 a comme nombre de versions max le nombre de base : 7"
    echo "- - - - - - - - - - - - - - - - - - - - - - "
        printf "TestPut1\n./Tests/TestPut1.txt\n\n{\"BashTest\":\"Put\",\"Type\":\"Put1\"}\n" | eval $put
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
        printf "TestPut2\n./Tests/TestPut2.txt\n2\n{\"BashTest\":\"Put\",\"Type\":\"Put2\"}\n" | eval $put
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
        printf "TestPut3\n./Tests/TestPut3.txt\n7\n{\"BashTest\":\"Put\",\"Type\":\"Put3\"}\n" | eval $put
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "Etat actuel : " 
        eval $list_obj
    echo "============================================"
    sleep $time
}

metadata_test () {
    echo "Métadonnées :"
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "==>> MGET"
    echo "On affiche les métadonnées de TestPut1"
        printf "TestPut1\n" | eval $mget
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "==>> MPUT"
    echo "On ajoute la métadonnée {'up1':'1'} puis {'up2':'2'} à TestPut1"
        printf "TestPut1\nup1\n1\n" | eval $mput
        printf "TestPut1\nup2\n2\n" | eval $mput
    echo "Vérification :"
        printf "TestPut1\n" | eval $mget
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "==>> LIST_KEY"
    echo "Existence de la clé 'Type' "
        printf "Type\n" | eval $list_key
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "==>> LIST_EXIST_VAL"
    echo "Existence de la valeur 'Put' "
        printf "Put\n\n" | eval $list_exist_val
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "==>> LIST_PART_KEY "
    echo "Existence d'une clé commençant par 'up' : "
        printf "up\n" | eval $list_part_key
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "==>> LIST_PART_VAL "
    echo "Existence d'une clé commençant par Ty pour une valeur commençant par 'Pu' : "
        printf "Ty\nPut\n" | eval $list_part_val
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "==>> LIST_KEY_REGEX "
    echo "TODO Existence d'une clé utilisant l'expression régulière [BasTeth] : "
        printf "[BasTeth]\n" | eval $list_key_regex
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "==>> LIST_VAL_REGEX"
    echo "TODO Existence d'une valeur utilisant l'expression régulière [^a] pour la clé et [2tuP] pour la valeur: "
        printf "[^a]\n[2tuP]\n" | eval $list_val_regex
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "==>> MDEL"
    echo "On supprime la métadonnée {'up1':'1'} ajoutée précédemment à TestPut1"
        printf "TestPut1\nup1\n" | eval $mdel
    echo "Vérification :"
        printf "TestPut1\n" | eval $mget
        sleep $time2
    echo "============================================"
    sleep $time   
}

maxVersion_test (){
    echo "Test du nombre max de versions"
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "==>> On exécute 10 fois l'ajout des objets TestPut1, TestPut2 et TestPut3"
        for i in {1..10}
        do
            echo "---> $i"
            printf "TestPut1\n./Tests/TestPut1.txt\n\n{\"BashTest\":\"Put\",\"Type\":\"Put1\"}\n" | eval $put
            printf "TestPut2\n./Tests/TestPut2.txt\n2\n{\"BashTest\":\"Put\",\"Type\":\"Put2\"}\n" | eval $put
            printf "TestPut3\n./Tests/TestPut3.txt\n7\n{\"BashTest\":\"Put\",\"Type\":\"Put3\"}\n" | eval $put
            sleep $time2
        done
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "==>> Listes des versions"
        echo ">> TestPut1 -> 5 versions"
            printf "TestPut1\n" | eval $list_version
            sleep $time2
        echo ">> TestPut2 -> 2 versions"
            printf "TestPut2\n" | eval $list_version
            sleep $time2
        echo ">> TestPut3 -> 7 versions"
            printf "TestPut3\n" | eval $list_version
            sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "==>> Réduction du nombre maximal de versions pour TestPut1: 5 -> 3"
        printf "TestPut1\n3\n" | eval $set_limit
    echo "Liste des versions de TestPut1 : Il doit y avoir 3 versions et non 5 comme au dessus"
        printf "TestPut1\n" | eval $list_version
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "==>> Définition globale du nombre maximal de versions à 5"
        printf "\n5\n" | eval $set_limit
    echo "==>> Listes des versions : Il doit y avoir 5 versions max par objet"
        echo ">> TestPut1"
            printf "TestPut1\n" | eval $list_version
            sleep $time2
        echo ">> TestPut2"
            printf "TestPut2\n" | eval $list_version
            sleep $time2
        echo ">> TestPut3"
            printf "TestPut3\n" | eval $list_version
            sleep $time2
    echo "============================================"
    sleep $time
}

delete_test () {
    echo "DELETE :"
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "Liste des versions de TestPut2"
        s=$(printf "TestPut2\n" | eval $list_version)
        first_ver=${s:26:19}
        echo "$s"
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "Suppression de la première version de TestPut2" 
        printf "TestPut2\n$first_ver\n" | eval $delete
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    
    echo "Liste des versions de TestPut2"
        printf "TestPut2\n" | eval $list_version
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "Suppression de TestPut3"
        printf "TestPut3\n\n" | eval $delete
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "Etat actuel : " 
    eval $list_obj
    echo "============================================"
    sleep $time
}

get_test () {
    echo "GET : "
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "Récupération de la dernière version de l'objet 'TestPut2'"
    printf "TestPut2\n\n" | eval $get
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "Affichage du fichier"
    cat TestPut2
    echo "============================================"

    echo "Vérification que deux versions sont bien différentes"
    echo "On ajoute un objet 'TestVer' basé sur le fichier TestPut2.txt"
        printf "TestVer\n./Tests/TestPut2.txt\n\n{\"BashTest\":\"Put\",\"Type\":\"PutVer1\"}\n" | eval $put
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "On ajoute une deuxième version de l'objet basée sur le fichier TestPut3.txt"
        printf "TestVer\n./Tests/TestPut3.txt\n\n{\"BashTest\":\"Put\",\"Type\":\"PutVer2\"}\n" | eval $put
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "On récupère chaque version"
    echo "Dernière version"
        printf "TestVer\n\n" | eval $get
        echo "Affichage du fichier obtenu"
        cat TestVer
    echo "Première version"
        printf "TestVer\n1\n" | eval $get
        echo "Affichage du fichier obtenu"
        cat TestVer
    echo "============================================"
    sleep $time
}

deduplication_test () {
    echo " Test de déduplication: "
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "On crée un objet TestPut1Dupl dont le contenu est une copie TestPut1-Copie.txt de testPut1.txt que TestPut1 mais les métadonnées sont {'TypeTest': 'Put1Dupl', 'Put1Dupl':'yes'}"
        printf "TestPut1Dupl\n./Tests/TestPut1-Copie.txt\n\n{\"TypeTest\":\"Put1Dupl\",\"Put1Dupl\":\"yes\"}\n" | eval $put
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "On récupère TestPut1Dupl et on affiche ses metadonnées"
        printf "TestPut1Dupl\n\n" | eval $get
        echo "Affichage du fichier : "
        cat TestPut1Dupl
        echo "Affichage des métadonnées"
        printf "TestPut1Dupl\n" | eval $mget
        sleep $time2
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "On récupère TestPut1 et on affiche ses metadonnées"
        s=$(printf "TestPut1\n" | eval $list_version)
        first_ver=${s:26:19}
        printf "TestPut1\n$first_ver\n" | eval $get
        echo "Affichage du fichier : "
        cat TestPut1
        echo "Affichage des métadonnées"
        printf "TestPut1\n" | eval $mget
        sleep $time2
    echo "============================================"
    sleep $time
}

batch_test(){
    echo " Transfert via l'opération batch: "
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "Ajout de TestPutBatch"
    python client.py -op batch -b '{"op": "put", "name": "TestPutBatch", "path": "./README.md", "maxVersion": 3, "meta": {"BashTest":"Put","Type":"PutBatch"}}' -u ${SERVER_URL}
    echo "Etat actuel : " 
        eval $list_obj
    echo "Versions"
        printf "TestPutBatch\n" | eval $list_version
    echo "Métadonées : "
        printf "TestPutBatch\n" | eval $mget
    echo "============================================"
}

all_tests () {
    put_test
    metadata_test
    maxVersion_test
    delete_test
    get_test
    deduplication_test
    batch_test
}

clean() {
    echo "Remise à zero du serveur..."
    rm -f ./TestPut1
    rm -f ./TestPut1Dupl
    rm -f ./TestPut2
    rm -f ./TestPut3
    rm -f ./TestVer
    rm -f ./obj/*
    rm -f ./objmeta/*
    echo "Done."
}

#exécute la fonction associée au premier argument donné
test_arg(){
    if [[ $1 == 'put' ]]
    then
    put_test
    elif [[ $1 == 'metadata' ]]
    then
    metadata_test
    elif [[ $1 == 'maxVersion' ]]
    then
    maxVersion_test
    elif [[ $1 == 'delete' ]]
    then
    delete_test
    elif [[ $1 == 'get' ]]
    then
    get_test
    elif [[ $1 == 'deduplication' ]]
    then
    deduplication_test
    elif [[ $1 == 'batch' ]]
    then
    batch_test
    elif [[ $1 == 'all' ]]
    then
    all_tests
    elif [[ $1 == 'clean' ]]
    then
    clean
    else
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    echo "L'argument donné n'est pas reconnu : $1 ."
    echo "- - - - - - - - - - - - - - - - - - - - - - "
    fi
}

if [[ $# -eq 0 ]]
then 
    echo "Aucun argument donné."
fi

for param in "$@"
do
    test_arg $param
done

